import IPort from "@/interface/IPort";
import IVolume from "@/interface/IVolume";
import ILink from "@/interface/ILink";
import IEnv from "@/interface/IEnv";

export default interface IImage {
    name: string;
    commonName: string;
    description: string;
    keywords: string[];
    versions: string[];
    link: string;
    color: string;
    envs: IEnv[];
    ports: IPort[];
    volumes: IVolume[];
    links: ILink[];
}
