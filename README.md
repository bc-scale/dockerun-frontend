# Dockerun

## Start new containers on the fly with ease

**Starting a brand new container is easy and you no longer have to remember each environments, ports and volume mappings.**

![](https://francescosorge.com/assets/website/dockerun/dockerun.png)

## Use it

- Dockerun is a public website available [here](https://dockerun.francescosorge.com).
- Dockerun is also available as a Docker image. Head over to [Docker Hub](https://hub.docker.com/r/fsorge/dockerun) to know more.
- _NEW!_ Dockerun can be installed on any Windows, macOS and Linux for offline usage via [dockerun-electron](https://gitlab.com/fsorge/dockerun-electron).

## Contribute

First of all, clone it using SSH

```bash
git clone git@gitlab.com:fsorge/dockerun-frontend.git
```

Then, install project dependencies:

```bash
npm install
```

To start the development server with hot-reload:

```bash
npm run serve
```

**That's all! Start doing your own experiments and improvements!**

**Note:** We suggest you to work in your own branch to avoid conflicts.

## Submit changes for review and approval

Before doing that, check that the website compiles correctly:

```bash
npm run build
```

If the website compiles correctly, it's time to submit a merge request. Use GitLab to do that.

The owner of the repository will then check your changes and if everything is right, your branch will be merged with the official website.
